FROM       sonatype/nexus
MAINTAINER Bernd Fischer <bfischer@mindapproach.de>

USER root

RUN usermod -u 1000 nexus
RUN chown -R nexus:nexus ${SONATYPE_WORK}

USER nexus

ENV CONTEXT_PATH /nexus
